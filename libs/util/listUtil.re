let tryRevMap(fn: 'a => Result.t('b, 'c), list: list('a)) = {
  let rec loop(image, remaining) = {
    switch remaining {
    | [] => Ok(image)
    | [item, ...rest] =>
      fn(item) |> Result.bind(_, output => {
        let image' = [output, ...image]
        loop(image', rest)
      })
    }
  }
  loop([], list)
}

let tryMap(fn, list) = {
  tryRevMap(fn, list) |> Result.map(List.rev)
}

let tryRevMapAsync(fn: 'a => Lwt.t('b), list: list('a)) = {
  let rec loop(image, remaining) = {
    switch remaining {
    | [] => Lwt.return(image)
    | [item, ...rest] =>
      fn(item) |> Lwt.bind(_, output => {
        let image' = [output, ...image]
        loop(image', rest)
      })
    }
  }
  loop([], list)
};

let tryMapAsync(fn, list) = {
  tryRevMapAsync(fn, list) |> Lwt.map(List.rev)
}

let tryFoldLeft(fn, init, list) = {
  let rec loop(acc, remaining) = {
    switch remaining {
    | [] => Ok(acc)
    | [item, ...rest] =>
      switch (fn(acc, item)) {
      | Ok(acc') =>
        loop(acc', rest)
      | Error(err) => Error(err)
      }
    }
  }
  loop(init, list)
}

