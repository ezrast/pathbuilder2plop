let dropSuffix(suf, str) = {
  if (String.ends_with(~suffix=suf, str)) {
    String.sub(str, 0, String.length(str) - String.length(suf))
  } else {
    str
  }
}

let dropPrefix(pref, str) = {
  if (String.starts_with(~prefix=pref, str)) {
    String.sub(str, String.length(pref), String.length(str) - String.length(pref))
  } else {
    str
  }
}
