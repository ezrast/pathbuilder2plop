let json_string = str_opt => {
  switch str_opt {
  | None => `Null
  | Some(str) => `String(str)
  }
}

let json_float = float_opt => {
  switch float_opt {
  | None => `Null
  | Some(float) => `Float(float)
  }
}

let json_bool = bool_opt => {
  switch bool_opt {
  | None => `Null
  | Some(bool) => `Bool(bool)
  }
}

let requireAssoc(json) = {
  switch json {
  | `Assoc(data) => Ok(data)
  | _ => Error(Failure("Value is not JSON object"))
  }
}

let requireList(json: Yojson.Safe.t) = {
  switch json {
  | `List(data) => Ok(data)
  | _ => Error(Failure("Value is not JSON array"))
  }
}

let requireString(json) = {
  switch json {
  | `String(data) => Ok(data)
  | _ => Error(Failure("Value is not JSON string"))
  }
}

let fromString(str) = {
  try (Ok(Yojson.Safe.from_string(str))) {
  | exn => Error(exn)
  }
}

module Infix {
  let (%)(json, idx) = {
    switch json {
    | `List(list) =>
      try (Ok(list |> List.nth(_, idx))) {
      | exn => Error(exn)
      }
    | _ => Error(Failure("Input is not a list"))
    }
  }

  let (&%)(result: result(Yojson.Safe.t, 'err), idx: int) = Result.bind(result, value => value % idx)

  let (%%)(json: Yojson.Safe.t, idx: string) = {
    switch json {
    | `Assoc(list) =>
      try (Ok(list |> List.assoc(idx))) {
      | exn => Error(exn)
      }
    | _ => Error(Failure("Input is not an object"))
    }
  }

  let (&%%)(result: result(Yojson.Safe.t, 'err), idx: string) = Result.bind(result, value => value %% idx)
}
