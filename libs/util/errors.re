let (let*)(result, fn) = {
  switch result {
  | Error(str) => Error(str)
  | Ok(value) => fn(value)
  }
}

exception PrefixException(string, exn)

let prefixErr(prefix, result) = {
  switch result {
  | Ok(value) => Ok(value)
  | Error(exn) => Error(PrefixException(prefix, exn))
  }
}

let exnToResult(fn) = {
  try (Ok(fn())) {
  | exn => Error(exn)
  }
}

let exnToResultBind(fn) = {
  try (fn()) {
  | exn => Error(exn)
  }
}

let rec getExnText(~prefix="", exn) = {
  switch exn {
  | Failure(str) => prefix ++ str
  | PrefixException(str, exn2) =>
    getExnText(~prefix=(str ++ ": "), exn2)
  | Ppx_yojson_conv_lib.Yojson_conv.Of_yojson_error(exn2, json) => "Error parsing " ++ (json |> Yojson.Safe.to_string) ++ ": " ++ getExnText(exn2)
  | _ => prefix ++ Printexc.to_string(exn)
  }
}
