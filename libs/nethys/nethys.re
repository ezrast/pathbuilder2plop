open Ppx_yojson_conv_lib.Yojson_conv.Primitives

module StringMap = Map.Make(String)

[@deriving yojson]
[@yojson.allow_extra_fields]
type item = {
  [@default 0.0] bulk: float,
  name: string,
  url: string,
  [@default ""] item_subcategory: string,
  [@default ""] actions: string,
  level: int,
  [@default ""] price_raw: string,
  [@default []] trait_raw: list(string),
  [@default ""] summary: string,
};

let emptyItem: item = {
  bulk: 0.0,
  name: "",
  url: "",
  item_subcategory: "",
  actions: "",
  level: 0,
  price_raw: "",
  trait_raw: [],
  summary: "",
};

[@deriving yojson]
[@yojson.allow_extra_fields]
type armor = {
  [@default 0.0] bulk: float,
  name: string,
  url: string,
  [@default ""] armor_category: string,
  [@default ""] armor_group: string,
  [@default []] trait_raw: list(string),
  [@default ""] summary: string,
  [@default 999] dex_cap: int,
  [@default 0] ac: int,
  [@default 0] check_penalty: int,
  [@default "0"] speed_penalty: string,
  [@default 0] strength: int,
};

let emptyArmor = {
  bulk: 0.0,
  name: "",
  url: "",
  armor_category: "",
  armor_group: "",
  trait_raw: [],
  summary: "",
  dex_cap: 999,
  ac: 0,
  check_penalty: 0,
  speed_penalty: "0 ft.",
  strength: 0,
};

[@deriving yojson]
[@yojson.allow_extra_fields]
type spell = {
  name: string,
  url: string,
  [@default []] component: list(string),
  [@default ""] range_raw: string,
  [@default ""] target: string,
  summary: string,
};

[@deriving yojson]
[@yojson.allow_extra_fields]
type weapon = {
  [@default 0.0] bulk: float,
  name: string,
  url: string,
  [@default []] trait: list(string),
  [@default []] trait_raw: list(string),
  [@default ""] damage: string,
  [@default 0] damage_die: int,
  weapon_category: string,
  weapon_type: string,
  summary: string,
};

type store = {
  fetch: string => Lwt.t(Yojson.Safe.t),
  mutable armorMemo: Hashtbl.t(string, Lwt.t(list(armor))),
  mutable itemMemo: Hashtbl.t(string, Lwt.t(list(item))),
  mutable spellMemo: Hashtbl.t(string, Lwt.t(list(spell))),
  mutable weaponMemo: Hashtbl.t(string, Lwt.t(list(weapon))),
}

let makeStore(fetchFn) = {
  fetch: fetchFn,
  armorMemo: Hashtbl.create(50),
  itemMemo: Hashtbl.create(50),
  spellMemo: Hashtbl.create(50),
  weaponMemo: Hashtbl.create(50),
}

let (let.)(lwt, fn) = {
  Lwt.bind(lwt, fn)
}

let (let.*)(lwt, fn) = {
  Lwt.bind(lwt, value => Lwt.of_result(fn(value)))
}

let hashtblMutex = Mutex.create()

let getThing(kind, name, memo, fetch, thing_of_yojson) = {
  Mutex.protect(hashtblMutex, () => {
    switch (Hashtbl.find_opt(memo, name)) {
    | Some(promise) => promise
    | None =>
      let promise = fetch(kind ++ "/" ++ name ++ ".json")
      |> Lwt.bind(_, json => {
        json
        |> Util.Json.requireList
        |> Result.map(List.map(thing_of_yojson))
        |> Lwt.of_result
      });
      Hashtbl.replace(memo, name, promise)
      promise
    }
  })
}

let getArmor(name, store) = getThing("armor", name, store.armorMemo, store.fetch, armor_of_yojson)
let getItem(name, store) = getThing("item", name, store.itemMemo, store.fetch, item_of_yojson)
let getSpell(name, store) = getThing("spell", name, store.spellMemo, store.fetch, spell_of_yojson)
let getWeapon(name, store) = getThing("weapon", name, store.weaponMemo, store.fetch, weapon_of_yojson)
