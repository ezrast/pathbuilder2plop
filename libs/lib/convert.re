open Ppx_yojson_conv_lib.Yojson_conv.Primitives

[@deriving yojson]
type equipmentContainer = {
  containerName: string,
  bagOfHolding: bool,
  backpack: bool,
}
module StringMap = Map.Make(String);

type equipmentContainerMap = StringMap.t(equipmentContainer);

let equipmentContainerMap_of_yojson(json) = {
  switch (json |> Util.Json.requireAssoc) {
  | Error(exn) => raise(exn)
  | Ok(assoc) =>
    assoc |> List.fold_left((acc, (uuid, container)) => {
      acc |> StringMap.add(uuid, equipmentContainer_of_yojson(container))
    }, StringMap.empty)
  }
}

type item = {
  name: string,
  quantity: int,
  location: string,
  invested: string,
}

let item_of_yojson(json) = {
  switch (json |> Util.Json.requireList) {
  | Error(exn) => raise(exn)
  | Ok([`String(name), `Int(quantity), `String(invested)]) => {name, quantity, location: "", invested}
  | Ok([`String(name), `Int(quantity), `String(location), `String(invested)]) => {name, quantity, location, invested}
  | Ok(_) => raise(Ppx_yojson_conv_lib.Yojson_conv.Of_yojson_error(Failure("Expected array with 3 or 4 items"), json))
  }
};

[@deriving of_yojson]
type pbMapLevelledBoosts = {
  [@default []][@key "1"] l1: list(string),
  [@default []][@key "5"] l5: list(string),
  [@default []][@key "10"] l10: list(string),
  [@default []][@key "15"] l15: list(string),
  [@default []][@key "20"] l20: list(string),
};

[@deriving of_yojson]
type pbBreakdown = {
  ancestryFree: list(string),
  ancestryBoosts: list(string),
  ancestryFlaws: list(string),
  backgroundBoosts: list(string),
  classBoosts: list(string),
  mapLevelledBoosts: pbMapLevelledBoosts,
};

type pbProficiencies = Hashtbl.t(string, int);
let pbProficiencies_of_yojson(yojson: Yojson.Safe.t) = {
  switch yojson {
  | `Assoc(assoc) =>
    let tbl = Hashtbl.create(34);
    assoc |> List.iter(((key, value)) => {
      switch value {
      | `Int(int) => Hashtbl.add(tbl, key, int)
      | _ => raise(Yojson.Json_error("proficiency " ++ key ++ " is not an int"))
      }
    });
    tbl
  | _ => raise(Yojson.Json_error("proficiencies must be a JSON object"))
  }
};

// [@deriving of_yojson]
type pbMods = unit;
let pbMods_of_yojson(_) = (); // TODO

[@deriving of_yojson]
type pbSpecificProficiencies = {
  trained: list(string),
  expert: list(string),
  master: list(string),
  legendary: list(string),
};

[@deriving of_yojson]
type pbWeapon = {
  name: string,
  qty: int,
  prof: string,
  die: string,
  pot: int,
  str: string,
  mat: unit, // TODO
  display: string,
  runes: list(unit), // TODO
  damageType: string,
  attack: int,
  damageBonus: int,
  extraDamage: list(unit), // TODO
};

[@deriving of_yojson]
type pbMoney = {
  cp: int,
  sp: int,
  gp: int,
  pp: int,
};

[@deriving of_yojson]
type pbArmor = {
  name: string,
  qty: int,
  prof: string,
  pot: int,
  res: string,
  mat: unit, // TODO
  display: string,
  worn: bool,
  runes: list(unit), // TODO
};

[@deriving of_yojson]
type focusList = {
  abilityBonus: int,
  proficiency: int,
  itemBonus: int,
  focusCantrips: list(string),
  focusSpells: list(string),
};

[@deriving of_yojson]
type focusAbilities = {
  [@key "con"][@default None] conFocus: option(focusList),
  [@key "int"][@default None] intFocus: option(focusList),
  [@key "wis"][@default None] wisFocus: option(focusList),
  [@key "cha"][@default None] chaFocus: option(focusList),
};

[@deriving of_yojson]
type pbFocus = {
  [@default None] arcane: option(focusAbilities),
  [@default None] divine: option(focusAbilities),
  [@default None] occult: option(focusAbilities),
  [@default None] primal: option(focusAbilities),
};

[@deriving of_yojson]
type pbFormula = {
  [@key "type"] typ: string,
  known: list(string),
};

[@deriving of_yojson]
type pbAcTotal = {
  acProfBonus: int,
  acAbilityBonus: int,
  acItemBonus: int,
  acTotal: int,
  shieldBonus: option(int),
};

[@deriving of_yojson]
type pbSpell = {
  spellLevel: int,
  list: list(string),
};

[@deriving of_yojson]
type pbSpellCaster = {
  name: string,
  magicTradition: string,
  spellcastingType: string,
  ability: string,
  proficiency: int,
  focusPoints: int,
  innate: bool,
  perDay: list(int),
  spells: list(pbSpell),
  prepared: list(unit), // TODO
  blendedSpells: list(unit), // TODO
};

[@deriving of_yojson]
type pbAbilities = {
  str: int,
  dex: int,
  con: int,
  int: int,
  wis: int,
  cha: int,
  breakdown: pbBreakdown,
};

[@deriving of_yojson]
type pbAttributes = {
  ancestryhp: int,
  classhp: int,
  bonushp: int,
  bonushpPerLevel: int,
  speed: int,
  speedBonus: int,
};

[@deriving of_yojson]
type pbBuild = {
  name: string,
  [@key "class"] klass: string,
  dualClass: option(string),
  level: int,
  ancestry: string,
  heritage: string,
  background: string,
  alignment: string,
  gender: string,
  age: string,
  deity: string,
  size: int,
  sizeName: string,
  keyability: string,
  languages: list(string),
  rituals: list(unit), // TODO
  resistances: list(unit), // TODO
  attributes: pbAttributes,
  abilities: pbAbilities,
  proficiencies: pbProficiencies,
  mods: pbMods, // TODO
  feats: list((string, option(string), string, int)),
  specials: list(string),
  lores: list((string, int)),
  equipmentContainers: equipmentContainerMap,
  equipment: list(item),
  specificProficiencies: pbSpecificProficiencies,
  weapons: list(pbWeapon),
  money: pbMoney,
  armor: list(pbArmor),
  spellCasters: list(pbSpellCaster),
  focusPoints: int,
  focus: pbFocus,
  formula: list(pbFormula),
  acTotal: pbAcTotal,
  pets: list(unit), // TODO
  familiars: list(unit), // TODO
};

[@deriving of_yojson]
type pbSheet = {
  success: bool,
  build: pbBuild,
};

let requireAssoc = Util.Json.requireAssoc;
let requireString = Util.Json.requireString;


[@deriving yojson]
type equipment = {
  name: string,
  quantity: int,
  location: string,
};

let (let.)(fn, lwt) = {
  Lwt.bind(fn, lwt)
}

let (let.*)(lwt, fn) = {
  Lwt.bind(lwt, value => Lwt.of_result(fn(value)))
};

let convert(pathbuilderData: Yojson.Safe.t, nethysStore: Nethys.store) = {
  open Plop.Sheet.Property;

  //
  // Utility functions
  //

  let warnings = ref([])
  let warningMutex = Mutex.create()
  let addWarning(exn) = {
    Mutex.protect(warningMutex, () => {
      warnings := [exn, ...warnings^]
    })
  }

  let charSucc(ch) = ch |> Char.code |> Int.succ |> Char.chr;

  let rec alphaIndex(idx): Seq.t(string) = () => {
    let charList = String.to_bytes(idx) |> Bytes.fold_right((ch, acc: (list(char), bool)) => {
      switch acc {
      | (chars, true) when ch >= 'Z' => (['A', ...chars], true)
      | (chars, true) => ([charSucc(ch), ...chars], false)
      | (chars, false) => ([ch, ...chars], false)
      }
    }, _, ([], true))

    let next = switch charList {
    | (chars, false) => chars
    | (chars, true) => ['A', ...chars]
    }
    |> List.to_seq
    |> String.of_seq

    Seq.Cons(next, alphaIndex(next))
  }
  let itemIndexes = alphaIndex("") |> Seq.take(52);
  let has(list, item) = List.find_opt((==)(item), list) != None

  let link(~text=?, path) = {
    let url = "https://2e.aonprd.com" ++ path;
    let text = text |> Option.value(~default=url);
    "<a target=\"_blank\" href=\"" ++ url ++ "\">" ++ text ++ "</a>"
  };

  //
  // Preprocessing
  //

  let pbSheet = pbSheet_of_yojson(pathbuilderData)
  let build = pbSheet.build
  let breakdown = build.abilities.breakdown;

  let spells = Array.make(11, [])

  build.spellCasters |> List.iter(sc => {
    sc.spells |> List.iter(spellList => {
      let level = spellList.spellLevel
      let newSpells = [spellList.list, ...Array.get(spells, level)]
      Array.set(spells, level, newSpells)
    })
  });

  let focusSpells = [build.focus.arcane, build.focus.divine, build.focus.occult, build.focus.primal]
  |> List.filter_map(item => item)
  |> List.concat_map(({conFocus, intFocus, wisFocus, chaFocus}) => {
    [conFocus, intFocus, wisFocus, chaFocus]
    |> List.filter_map(item => item)
    |> List.concat_map(focusList => {focusList.focusSpells})
  })

  let focusCantrips = [build.focus.arcane, build.focus.divine, build.focus.occult, build.focus.primal]
  |> List.filter_map(item => item)
  |> List.concat_map(({conFocus, intFocus, wisFocus, chaFocus}) => {
    [conFocus, intFocus, wisFocus, chaFocus]
    |> List.filter_map(item => item)
    |> List.concat_map(focusList => {focusList.focusCantrips})
  })

  //
  // Set up promises
  //

  // TODO add weapons and armor to inventory
  let equipmentPromise = build.equipment |> Lwt_list.map_p((item: item) => {
    Lwt.try_bind(
      () => Nethys.getItem(item.name, nethysStore),
      itemDetails => Lwt.return((item, itemDetails |> List.hd)), // TODO resolve multi-item lists
      exn => {
        addWarning(Util.Errors.PrefixException("Error loading " ++ item.name, exn))
        let dummyItem: Nethys.item = {...Nethys.emptyItem, name: item.name};
        Lwt.return((item, dummyItem))
      }
    )
  });

  let weaponPromise = build.weapons |> Lwt_list.map_p((weapon: pbWeapon) => {
    Lwt.try_bind(
      () => Nethys.getWeapon(weapon.name, nethysStore),
      list => Lwt.return(switch list { | [] => None | [hd, ..._] => Some(hd) }),
      exn => {
        addWarning(Util.Errors.PrefixException("Error loading " ++ weapon.name, exn))
        Lwt.return(None)
      }
    )
  })

  let armorPromise = switch (build.armor) {
  | [] => Nethys.getArmor("Unarmored", nethysStore)
  | [hd, ..._tl] =>
    Lwt.catch(
      () => Nethys.getArmor(hd.name, nethysStore),
      exn1 => Lwt.catch(
        () => Nethys.getArmor(hd.name ++ " Armor", nethysStore),
        exn2 => {
          addWarning(Util.Errors.PrefixException("Error loading " ++ hd.name, exn1));
          addWarning(Util.Errors.PrefixException("Error loading " ++ hd.name ++ " Armor", exn2));
          Lwt.return([Nethys.emptyArmor])
        }
      )

    )
  };

  let spellListPromise(title, spellList) = {
    let. spellDescriptions = spellList |> Lwt_list.filter_map_p(spellName => {
      Lwt.try_bind(
        () => Nethys.getSpell(spellName, nethysStore),
        spellDataList => {
          let spellData = spellDataList |> List.hd; // TODO resolve multi-item lists
          let p1 = [
            "<b>" ++ spellName ++ "</b>",
            spellData.component |> String.concat(", "),
            spellData.range_raw,
            spellData.target,
          ] |> List.filter((!=)("")) |> String.concat("; ");
          let p2 = [
            spellData.summary,
            if (spellData.url == "") { "" } else { link(spellData.url) },
          ] |> List.filter((!=)("")) |> String.concat(" - ");

          Lwt.return(Some(paragraph("<p>" ++ p1 ++ "</p>" ++ (if ( p2 == "") { "" } else { "<p>" ++ p2 ++ "</p2>" }))))
        },
        exn => {
          addWarning(Util.Errors.PrefixException("Error loading " ++ spellName, exn))
          Lwt.return(None)
        }
      )
    });
    Lwt.return([
      heading(title),
      ...spellDescriptions,
    ])
  }

  let alchemyPromise = build.formula
  |> List.concat_map(pbFormula => pbFormula.known)
  |> Lwt_list.filter_map_p(itemName => {
    Lwt.try_bind(
      () => Nethys.getItem(itemName, nethysStore),
      itemDataList => {
        let itemData = itemDataList |> List.hd; // TODO resolve multi-item lists
        let p1 = [
          "<b>" ++ itemName ++ "</b>",
          if (itemData.item_subcategory != "") { "(" ++ itemData.item_subcategory ++ ")" } else { "" },
          itemData.trait_raw |> String.concat(", "),
          itemData.actions,
        ] |> List.filter((!=)("")) |> String.concat("; ");
        let p2 = [
          // Summaries for alchemical items are often misleading because they
          // are merely minor/lesser/etc variants of some base type
          "Level " ++ string_of_int(itemData.level),
          if (itemData.bulk > 0.0) { string_of_float(itemData.bulk) ++ " bulk" } else { "" },
          itemData.price_raw,
          if (itemData.url == "") { "" } else { link(itemData.url) },
        ] |> List.filter((!=)("")) |> String.concat("; ");

        Lwt.return(Some(paragraph("<p>" ++ p1 ++ "</p>" ++ (if ( p2 == "") { "" } else { "<p>" ++ p2 ++ "</p2>" }))))
      },
      exn => {
        addWarning(Util.Errors.PrefixException("Error loading " ++ itemName, exn))
        Lwt.return(None)
      }
    )
  });

  let cantrips = Array.get(spells, 0);
  let cantripSpellPromise = if (List.length(cantrips) == 0) { None } else { Some(spellListPromise("Cantrips", List.flatten(cantrips))) };
  let focusCantripPromise = if (List.length(focusCantrips) == 0) { None } else { Some(spellListPromise("Focus Cantrips", focusCantrips)) };
  let focusSpellPromise = if (List.length(focusSpells) == 0) { None } else { Some(spellListPromise("Focus Spells", focusSpells)) };
  let leveledSpellPromise = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] |> List.map(level => {
    let spells' = Array.get(spells, level);
    if (List.length(spells') == 0) { None } else { Some(spellListPromise("Level " ++ string_of_int(level) ++ " spells", List.flatten(spells'))) };
  });

  //
  // Fulfill promises
  //

  let. equipmentWithDetails = equipmentPromise;
  let. weapons = weaponPromise;
  let. armorDataList = armorPromise;
  let. spellProperties = [cantripSpellPromise, focusCantripPromise, focusSpellPromise, ...leveledSpellPromise]
  |> List.filter_map(item => item)
  |> Lwt_list.map_p(item => item)
  |> Lwt.map(List.flatten);

  let. alchemyProperties = alchemyPromise;

  let. armorData = switch armorDataList {
  | [] =>
    addWarning(Failure("Armor had no data; this could be an asset compilation error"))
    Lwt.return(Nethys.emptyArmor)
  | [hd, ..._tl] => Lwt.return(hd)
  };


  // We want our equipment tab to have a fixed number of slots, and Seq.zip
  // stops with the shortest sequence, so ensure our seq of equipment indexes
  // is longer than our seq of equipment by padding with None
  let equipmentSeqForever = Seq.append(equipmentWithDetails |> List.to_seq |> Seq.map(item => Some(item)), Seq.repeat(None));
  let indexedWeapons = weapons
  |> List.to_seq
  |> Seq.filter_map(item => item)
  |> Seq.map2((aa, bb) => (aa, bb), itemIndexes);

  let proficiencyFormula(name) = "level + 2 * (" ++ name ++ "-trained + " ++ name ++ "-expert + " ++ name ++ "-master + " ++ name ++ "-legendary)"

  let weaponProperties = indexedWeapons |> Seq.map(((idx, weaponData: Nethys.weapon)) => {
    let proficiency = [
      (Hashtbl.find_opt(build.proficiencies, String.lowercase_ascii(weaponData.weapon_category)) |> Option.value(~default=0)) / 2,
      List.find_opt((==)(weaponData.name), build.specificProficiencies.trained) != None ? 1 : 0,
      List.find_opt((==)(weaponData.name), build.specificProficiencies.expert) != None ? 2 : 0,
      List.find_opt((==)(weaponData.name), build.specificProficiencies.master) != None ? 3 : 0,
      List.find_opt((==)(weaponData.name), build.specificProficiencies.legendary) != None ? 4 : 0,
    ] |> List.fold_left(Int.max, Int.min_int)
    let ranged = weaponData.weapon_type == "Ranged"
    let damageDie = number("Damage Die Size " ++ idx, ~value=float_of_int(weaponData.damage_die))
    let deadlyDie = weaponData.trait_raw |> List.find_map(trait => {
      if (String.starts_with(~prefix="Deadly ", trait)) {
        Some(trait
          |> Util.StringUtil.dropPrefix("Deadly ")
          |> Util.StringUtil.dropPrefix("1")
          |> Util.StringUtil.dropPrefix("d")
          |> float_of_string
        )
      } else {
        None
      }
    })
    |> Option.value(~default=0.0)
    |> number("Deadly Die Size " ++ idx, ~value=_)

    let fatalDie = weaponData.trait_raw |> List.find_map(trait => {
      if (String.starts_with(~prefix="Fatal ", trait)) {
        Some(trait
          |> Util.StringUtil.dropPrefix("Fatal ")
          |> Util.StringUtil.dropPrefix("1")
          |> Util.StringUtil.dropPrefix("d")
          |> float_of_string
        )
      } else {
        None
      }
    })
    |> Option.value(~default=0.0)
    |> number("Fatal Die Size " ++ idx, ~value=_)

    let thrown = checkbox("Thrown " ++ idx, ~value=List.find_opt((==)("Thrown"), weaponData.trait) != None)
    let propulsive = checkbox("Propulsive " ++ idx, ~value=List.find_opt((==)("Propulsive"), weaponData.trait) != None)
    let damageBonus = number(
      "Damage Bonus " ++ idx,
      ~formula=if (ranged) {
        "Thrown_" ++ idx ++ " * Strength + Propulsive_" ++ idx ++ " * (Strength > 0 ? floor(Strength / 2) : Strength)"
      } else {
        "max(Strength, Finesse_" ++ idx ++ " * Dex_to_Finesse_Damage * Dexterity)"
      }
    );

    let statText = [weaponData.damage, weaponData.weapon_category, String.concat(", ", weaponData.trait_raw)]
    |> List.filter((!=)(""))
    |> String.concat("; ")
    let statText = if (statText == "") { "" } else { "<p>" ++ statText ++ "</p>" };
    let summary = if (weaponData.summary == "") { "" } else { "<p>" ++ weaponData.summary ++ "</p>" };
    let linkUrl = if (weaponData.url == "") { "" } else { "<p>" ++ link(weaponData.url) ++ "</p>" };

    let skill = skill4("Proficiency " ++ idx, ~checked=proficiency, ~formula=proficiencyFormula("Proficiency_" ++ idx))
    let finesse = checkbox("Finesse " ++ idx, ~value=List.find_opt((==)("Finesse"), weaponData.trait) != None)
    let mapAdjustment = number("MAP Adjustment " ++ idx, ~value=if (List.find_opt((==)("Agile"), weaponData.trait) == None) { 0.0 } else { 1.0 })
    let attackBonus = number(
      "Attack Bonus " ++ idx,
      ~formula=if (ranged) { "Proficiency_" ++ idx ++ " + Dexterity" } else { "Proficiency_" ++ idx ++ " + max(Strength, Finesse_" ++ idx ++ " * Dexterity)" }
    );

    titleSection(weaponData.name, [
      paragraph("<h3>" ++ weaponData.name ++ "</h3>" ++ statText ++ summary ++ linkUrl),
      horizontalSection([
        section([skill, finesse, mapAdjustment, attackBonus]),
        section([damageDie, deadlyDie, fatalDie, thrown, propulsive, damageBonus]),
      ]),
    ])
  }) |> List.of_seq;

  let makeProficiency(name, checked, stat, displayStat) = {
    let cleaned = name |> String.map(chr => if (chr == ' ') { '_' } else { chr })
    skill4(
      name,
      ~checked=checked,
      ~formula=stat ++ " + " ++ proficiencyFormula(cleaned),
      ~message=name ++ ": {d20 + " ++ cleaned ++ "}",
      ~subtitle=displayStat
    )
  };

  let sheet = [
      tabSection("Vitals", [
        horizontalSection([
          section(~size=65.0, [
            health("Hit-Points", ~formula="MaximumHitPoints"),
            horizontalSection([
              section(~size=30.0, [
                number("Speed", ~formula="AncestrySpeed + BonusSpeed + Effective_Speed_Penalty"),
                number("Armor-Class", ~formula="10 + min(Dexterity, Dex_Cap) + AC_Bonus + Armor_Proficiency"),
              ]),
              section(~size=35.0, [
                makeProficiency("Perception", Hashtbl.find(build.proficiencies, "perception") / 2, "Wisdom", "Wis"),
                makeProficiency("Fortitude", Hashtbl.find(build.proficiencies, "fortitude") / 2, "Constitution", "Con"),
                makeProficiency("Reflex", Hashtbl.find(build.proficiencies, "reflex") / 2, "Dexterity", "Dex"),
                makeProficiency("Will", Hashtbl.find(build.proficiencies, "will") / 2, "Wisdom", "Wis"),
                skill4(
                  "Class DC",
                  ~checked=Hashtbl.find(build.proficiencies, "classDC") / 2,
                  ~formula=proficiencyFormula("Class_DC") ++ " + Strength * str-c + Dexterity * dex-c + Constitution * con-c + Intelligence * int-c + Wisdom * wis-c + Charisma * cha-c",
                )
              ]),
            ]),
            titleSection("Attacks", [
              horizontalSection([
                section(~size=30.0, [number("Base MAP", ~value=-5.0)]),
                section(~size=35.0, [checkboxes("Attacks this round", 2.0)]),
              ]),
              ...(indexedWeapons |> Seq.concat_map(((idx, weaponData: Nethys.weapon)) => {
                [
                  number(weaponData.name ++ " Attack", ~formula="Attack_Bonus_" ++ idx ++ " + Attacks_this_round * (Base_MAP + MAP_Adjustment_" ++ idx ++ ")", ~message="Attack: {d20 + Weapon_" ++ idx ++ "_Attack}"),
                  ability(
                    weaponData.name ++ " Damage",
                    ~value="!",
                    ~message="Damage: {d@:Damage_Die_Size_" ++ idx ++ ": + Damage_Bonus_" ++ idx ++ "}"),
                  ability(
                    weaponData.name ++ " Crit",
                    ~value="!!",
                    ~message="Damage: {2 * (d@:max(Damage_Die_Size_" ++ idx ++ ",Fatal_Die_Size_" ++ idx ++ "): + Damage_Bonus_" ++ idx ++ ") "
                      ++ " + d@:Fatal_Die_Size_" ++ idx ++ ": + d@:Deadly_Die_Size_" ++ idx ++ ":}"),
                  // TODO handle multiple damage dice
                ] |> List.to_seq
              }) |> List.of_seq)
            ])
          ]),
          section(~size=35.0, [
            heading("Skills"),
            makeProficiency("Acrobatics", Hashtbl.find(build.proficiencies, "acrobatics") / 2, "Effective_Dexterity_for_Skills", "Dex"),
            makeProficiency("Arcana", Hashtbl.find(build.proficiencies, "arcana") / 2, "Intelligence", "Int"),
            makeProficiency("Athletics", Hashtbl.find(build.proficiencies, "athletics") / 2, "Effective_Strength_for_Skills", "Str"),
            makeProficiency("Crafting", Hashtbl.find(build.proficiencies, "crafting") / 2, "Intelligence", "Int"),
            makeProficiency("Deception", Hashtbl.find(build.proficiencies, "deception") / 2, "Charisma", "Cha"),
            makeProficiency("Diplomacy", Hashtbl.find(build.proficiencies, "diplomacy") / 2, "Charisma", "Cha"),
            makeProficiency("Intimidation", Hashtbl.find(build.proficiencies, "intimidation") / 2, "Charisma", "Cha"),
            makeProficiency("Medicine", Hashtbl.find(build.proficiencies, "medicine") / 2, "Wisdom", "Wis"),
            makeProficiency("Nature", Hashtbl.find(build.proficiencies, "nature") / 2, "Wisdom", "Wis"),
            makeProficiency("Occultism", Hashtbl.find(build.proficiencies, "occultism") / 2, "Intelligence", "Int"),
            makeProficiency("Performance", Hashtbl.find(build.proficiencies, "performance") / 2, "Charisma", "Cha"),
            makeProficiency("Religion", Hashtbl.find(build.proficiencies, "religion") / 2, "Wisdom", "Wis"),
            makeProficiency("Society", Hashtbl.find(build.proficiencies, "society") / 2, "Intelligence", "Int"),
            makeProficiency("Stealth", Hashtbl.find(build.proficiencies, "stealth") / 2, "Effective_Dexterity_for_Skills", "Dex"),
            makeProficiency("Survival", Hashtbl.find(build.proficiencies, "survival") / 2, "Wisdom", "Wis"),
            makeProficiency("Thievery", Hashtbl.find(build.proficiencies, "thievery") / 2, "Effective_Dexterity_for_Skills", "Dex"),
            heading("Lores"),
            ...build.lores |> List.map(((name, value)) => {
              makeProficiency(name ++ " Lore", value, "Intelligence", "Int")
            })
          ]),
        ]),
      ]),
      tabSection("Features", [
        heading("Special"),
        table(
          ["Name", "Description"],
          build.specials |> List.concat_map(name => {
            [text("", name), paragraph("<p>Not yet implemented</p>")]
          }),
        ),
        heading("Feats"),
        table(["Level", "Name", "Type", "Description"],
          build.feats |> List.concat_map(((name: string, sub, kind: string, level: int)) => {
            let sub = sub |> Option.map(text => " (" ++ text ++ ")") |> Option.value(~default="");
            [number("", ~value=float_of_int(level)), text("", name ++ sub), text("", kind), paragraph("<p>Not yet implemented</p>")]
          })
        )
      ]),
      tabSection("Equipment", [
        checkbox("Dex to Finesse Damage", ~value=(build.specials |> List.find_opt((==)("Thief Racket")) != None)),
        section(weaponProperties),
        titleSection("Armor", [
          text("Name", armorData.name),
          {
            let statText = [armorData.armor_category, armorData.armor_group, String.concat(", ", armorData.trait_raw)]
            |> List.filter((!=)(""))
            |> String.concat("; ");
            let statText = if (statText == "") { "" } else { "<p>" ++ statText ++ "</p>" };
            let summary = if (armorData.summary == "") { "" } else { "<p>" ++ armorData.summary ++ "</p>" };
            let linkUrl = if (armorData.url == "") { "" } else { "<p>" ++ link(armorData.url) ++ "</p>" }
            paragraph("<h3>" ++ armorData.name ++ "</h3>" ++ statText ++ summary ++ linkUrl)
          },
          skill4(
            "Armor Proficiency",
            ~checked=Hashtbl.find_opt(build.proficiencies, String.lowercase_ascii(armorData.armor_category)) |> Option.value(~default=0) |> (/)(_, 2),
            ~formula=proficiencyFormula("Armor_Proficiency"),
          ),
          number("Dex Cap", ~value=float_of_int(armorData.dex_cap)),
          number("AC Bonus", ~value=float_of_int(armorData.ac)),
          number("Check Penalty", ~value=float_of_int(armorData.check_penalty)),
          number("Speed Penalty", ~value=float_of_string(armorData.speed_penalty |> Util.StringUtil.dropSuffix(" ft."))),
          number("Strength Threshold", ~value=float_of_int(armorData.strength)),
          number("Effective Check Penalty", ~value=0.0, ~formula="Strength-score < Strength_Threshold ? Check_Penalty : 0"),
          number("Effective Speed Penalty", ~value=0.0, ~formula="Strength-score < Strength_Threshold ? min(0, Speed_Penalty + 5) : 0"),
          number("Effective Strength for Skills", ~value=0.0, ~formula="Strength + Effective_Check_Penalty"),
          number("Effective Dexterity for Skills", ~value=0.0, ~formula="Dexterity + Effective_Check_Penalty"),
        ]),
        titleSection("Proficiencies", [
          [
            skill4("Unarmored", ~checked=Hashtbl.find(build.proficiencies, "unarmored") / 2, ~formula=proficiencyFormula("Unarmored")),
            skill4("Light Armor", ~checked=Hashtbl.find(build.proficiencies, "light") / 2, ~formula=proficiencyFormula("Light_Armor")),
            skill4("Medium Armor", ~checked=Hashtbl.find(build.proficiencies, "medium") / 2, ~formula=proficiencyFormula("Medium_Armor")),
            skill4("Heavy Armor", ~checked=Hashtbl.find(build.proficiencies, "heavy") / 2, ~formula=proficiencyFormula("Heavy_Armor")),

            skill4("Unarmed", ~checked=Hashtbl.find(build.proficiencies, "unarmed") / 2, ~formula=proficiencyFormula("Unarmed")),
            skill4("Simple Weapons", ~checked=Hashtbl.find(build.proficiencies, "simple") / 2, ~formula=proficiencyFormula("Simple_Weapons")),
            skill4("Martial Weapons", ~checked=Hashtbl.find(build.proficiencies, "martial") / 2, ~formula=proficiencyFormula("Martial_Weapons")),
            skill4("Advanced Weaponso", ~checked=Hashtbl.find(build.proficiencies, "advanced") / 2, ~formula=proficiencyFormula("Advanced_Weaponso")),
          ],
          build.specificProficiencies.trained |> List.map(name => skill4(name, ~checked=1)),
          build.specificProficiencies.expert |> List.map(name => skill4(name, ~checked=2)),
          build.specificProficiencies.master |> List.map(name => skill4(name, ~checked=3)),
          build.specificProficiencies.legendary |> List.map(name => skill4(name, ~checked=4)),
        ] |> List.flatten),
      ]),
      tabSection("Preparations", [
        // TODO spells/infusions per day tracker
        titleSection("Spells", spellProperties),
        titleSection(
            "Spell Proficiencies",
            [
              skill4("Arcane Casting", ~checked=Hashtbl.find(build.proficiencies, "castingArcane") / 2, ~formula=proficiencyFormula("Arcane_Casting")),
              skill4("Divine Casting", ~checked=Hashtbl.find(build.proficiencies, "castingDivine") / 2, ~formula=proficiencyFormula("Divine_Casting")),
              skill4("Occult Casting", ~checked=Hashtbl.find(build.proficiencies, "castingOccult") / 2, ~formula=proficiencyFormula("Occult_Casting")),
              skill4("Primal Casting", ~checked=Hashtbl.find(build.proficiencies, "castingPrimal") / 2, ~formula=proficiencyFormula("Primal_Casting")),
            ]
          ),
        titleSection("Alchemy", alchemyProperties),
      ]),
      tabSection("Inventory", [
      horizontalSection([
        text("Total Bulk", "", ~formula=(itemIndexes |> Seq.map(idx => "quant_" ++ idx ++ " * bulk_" ++ idx) |> List.of_seq |> String.concat(" + "))),
        text("Carrying Capacity", "", ~formula="Strength + 5"),
      ]),
      table(
        ["Item", "Quantity", "Bulk per"],
        Seq.zip(itemIndexes, equipmentSeqForever) |> Seq.flat_map(((stringIdx, itemOpt)) => {
          switch itemOpt {
          | Some((item: item, itemDetails: Nethys.item)) =>
            let container = build.equipmentContainers |> StringMap.find_opt(item.location)
            let containerText = container |> Option.map(cnt => " (" ++ cnt.containerName ++ ")") |> Option.value(~default="")
            let paragraphText =
              (if (itemDetails.url == "") { item.name } else { link(~text=item.name, itemDetails.url) }) ++ containerText;
            [
              paragraph(paragraphText),
              number("quant_" ++ stringIdx, ~value=float_of_int(item.quantity)),
              number("bulk_" ++ stringIdx, ~value=itemDetails.bulk)
            ] |> List.to_seq
          | None => [paragraph(""), number("quant_" ++ stringIdx), number("bulk_" ++ stringIdx, ~value=0.0)] |> List.to_seq
          }
        }) |> List.of_seq
      )
    ]),
    tabSection("Bio", [
      horizontalSection([
        section(~size=70.0, [
          heading(build.name),
          text("Class", build.klass),
          text("Ancestry", build.ancestry),
          text("Background", build.background),
          heading("Abilities"),
          ability(
            ~scoreFormula="f(x) = x > 5 ? floor(x/2 + 2.5) : x; 10 + 2 * f(str-a + str-b + str-c + str-v + str-w + str-x + str-y + str-z - str-f)",
            ~formula="floor(Strength-score / 2) - 5",
            ~message="{d20 + Strength}",
            "Strength",
          ),
          ability(
            ~scoreFormula="f(x) = x > 5 ? floor(x/2 + 2.5) : x; 10 + 2 * f(dex-a + dex-b + dex-c + dex-v + dex-w + dex-x + dex-y + dex-z - dex-f)",
            ~formula="floor(Dexterity-score / 2) - 5",
            ~message="{d20 + Dexterity}",
            "Dexterity",
          ),
          ability(
            ~scoreFormula="f(x) = x > 5 ? floor(x/2 + 2.5) : x; 10 + 2 * f(con-a + con-b + con-c + con-v + con-w + con-x + con-y + con-z - con-f)",
            ~formula="floor(Constitution-score / 2) - 5",
            ~message="{d20 + Constitution}",
            "Constitution",
          ),
          ability(
            ~scoreFormula="f(x) = x > 5 ? floor(x/2 + 2.5) : x; 10 + 2 * f(int-a + int-b + int-c + int-v + int-w + int-x + int-y + int-z - int-f)",
            ~formula="floor(Intelligence-score / 2) - 5",
            ~message="{d20 + Intelligence}",
            "Intelligence",
          ),
          ability(
            ~scoreFormula="f(x) = x > 5 ? floor(x/2 + 2.5) : x; 10 + 2 * f(wis-a + wis-b + wis-c + wis-v + wis-w + wis-x + wis-y + wis-z - wis-f)",
            ~formula="floor(Wisdom-score / 2) - 5",
            ~message="{d20 + Wisdom}",
            "Wisdom",
          ),
          ability(
            ~scoreFormula="f(x) = x > 5 ? floor(x/2 + 2.5) : x; 10 + 2 * f(cha-a + cha-b + cha-c + cha-v + cha-w + cha-x + cha-y + cha-z - cha-f)",
            ~formula="floor(Charisma-score / 2) - 5",
            ~message="{d20 + Charisma}",
            "Charisma",
          ),
        ]),
        section([
          number(~value=float_of_int(build.level), "Level"),
          paragraph("\
<p>Age: " ++ build.age ++ "</p>
<p>Gender: " ++ build.gender ++ "</p>
<p>Deity: " ++ build.deity ++ "</p>
<p>Size: " ++ build.sizeName ++ "</p>
<p>Heritage: " ++ build.heritage ++ "</p>
<p>Languages: " ++ (build.languages |> String.concat(", ")) ++ "</p>"
          )
        ])
      ]),
      titleSection(~collapsed=true, "Ability Boosts & Flaws", [
        table(["Ancestry", "Background", "Class", "Flaw"], [
          checkbox(~value=(has(breakdown.ancestryBoosts, "Str") || has(breakdown.ancestryFree, "Str")), "str-a"),
          checkbox(~value=(has(breakdown.backgroundBoosts, "Str")), "str-b"),
          checkbox(~value=(has(breakdown.classBoosts, "Str")), "str-c"),
          checkbox(~value=(has(breakdown.ancestryFlaws, "Str")), "str-f"),

          checkbox(~value=(has(breakdown.ancestryBoosts, "Dex") || has(breakdown.ancestryFree, "Dex")), "dex-a"),
          checkbox(~value=(has(breakdown.backgroundBoosts, "Dex")), "dex-b"),
          checkbox(~value=(has(breakdown.classBoosts, "Dex")), "dex-c"),
          checkbox(~value=(has(breakdown.ancestryFlaws, "Dex")), "dex-f"),

          checkbox(~value=(has(breakdown.ancestryBoosts, "Con") || has(breakdown.ancestryFree, "Con")), "con-a"),
          checkbox(~value=(has(breakdown.backgroundBoosts, "Con")), "con-b"),
          checkbox(~value=(has(breakdown.classBoosts, "Con")), "con-c"),
          checkbox(~value=(has(breakdown.ancestryFlaws, "Con")), "con-f"),

          checkbox(~value=(has(breakdown.ancestryBoosts, "Int") || has(breakdown.ancestryFree, "Int")), "int-a"),
          checkbox(~value=(has(breakdown.backgroundBoosts, "Int")), "int-b"),
          checkbox(~value=(has(breakdown.classBoosts, "Int")), "int-c"),
          checkbox(~value=(has(breakdown.ancestryFlaws, "Int")), "int-f"),

          checkbox(~value=(has(breakdown.ancestryBoosts, "Wis") || has(breakdown.ancestryFree, "Wis")), "wis-a"),
          checkbox(~value=(has(breakdown.backgroundBoosts, "Wis")), "wis-b"),
          checkbox(~value=(has(breakdown.classBoosts, "Wis")), "wis-c"),
          checkbox(~value=(has(breakdown.ancestryFlaws, "Wis")), "wis-f"),

          checkbox(~value=(has(breakdown.ancestryBoosts, "Cha") || has(breakdown.ancestryFree, "Cha")), "cha-a"),
          checkbox(~value=(has(breakdown.backgroundBoosts, "Cha")), "cha-b"),
          checkbox(~value=(has(breakdown.classBoosts, "Cha")), "cha-c"),
          checkbox(~value=(has(breakdown.ancestryFlaws, "Cha")), "cha-f"),
        ]),
        table(["Level 1", "5", "10", "15", "20"], [
          checkbox(~value=has(breakdown.mapLevelledBoosts.l1, "Str"), "str-v"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l5, "Str"), "str-w"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l10, "Str"), "str-x"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l15, "Str"), "str-y"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l20, "Str"), "str-z"),

          checkbox(~value=has(breakdown.mapLevelledBoosts.l1, "Dex"), "dex-v"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l5, "Dex"), "dex-w"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l10, "Dex"), "dex-x"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l15, "Dex"), "dex-y"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l20, "Dex"), "dex-z"),

          checkbox(~value=has(breakdown.mapLevelledBoosts.l1, "Con"), "con-v"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l5, "Con"), "con-w"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l10, "Con"), "con-x"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l15, "Con"), "con-y"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l20, "Con"), "con-z"),

          checkbox(~value=has(breakdown.mapLevelledBoosts.l1, "Int"), "int-v"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l5, "Int"), "int-w"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l10, "Int"), "int-x"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l15, "Int"), "int-y"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l20, "Int"), "int-z"),

          checkbox(~value=has(breakdown.mapLevelledBoosts.l1, "Wis"), "wis-v"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l5, "Wis"), "wis-w"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l10, "Wis"), "wis-x"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l15, "Wis"), "wis-y"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l20, "Wis"), "wis-z"),

          checkbox(~value=has(breakdown.mapLevelledBoosts.l1, "Cha"), "cha-v"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l5, "Cha"), "cha-w"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l10, "Cha"), "cha-x"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l15, "Cha"), "cha-y"),
          checkbox(~value=has(breakdown.mapLevelledBoosts.l20, "Cha"), "cha-z"),
        ])
      ]),
      titleSection(~collapsed=true, "Hit Points", [
        number(~value=float_of_int(build.attributes.ancestryhp), "Ancestry HP"),
        number(~value=float_of_int(build.attributes.classhp), "Class HP per level"),
        number(~value=float_of_int(build.attributes.bonushp), "Bonus HP"),
        number(~value=float_of_int(build.attributes.bonushpPerLevel), "Bonus HP Per Level"),
        number(~value=0.0, "Maximum Hit Points", ~formula="AncestryHp + BonusHP + level * max(1, ClassHPPerLevel + BonusHPPerLevel + Constitution)"),
      ]),
      titleSection(~collapsed=true, "Speed", [
        number(~value=float_of_int(build.attributes.speed), "Ancestry Speed"),
        number(~value=float_of_int(build.attributes.speedBonus), "Bonus Speed"),
      ])
    ])
  ]
  Lwt.return((sheet |> Plop.Sheet.make_json, warnings^))
}
