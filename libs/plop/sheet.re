module Property {
  open Util.Json

  type t = {
    children: list(t),
    data: Yojson.Safe.t,
    typ: string,
    formula: option(string),
    message: option(string),
    name: string,
    size: option(float),
    value: Yojson.Safe.t,
  }

  let rec ability = (~scoreFormula=?, ~scoreValue=?, ~formula=?, ~message=?, ~value=?, name) => {
    children: if (scoreValue == None && scoreFormula == None) {
      [];
    } else {
      [number(~value=?scoreValue, ~formula=?scoreFormula, name ++ "-score")];
    },
    data: `Null,
    typ: "ability",
    formula: formula,
    message: message,
    name: name,
    size: None,
    value: json_string(value),
  }

  and appearance = {
    children: [],
    data: `Assoc([("appearances", `List([]))]),
    typ: "appearance",
    formula: None,
    message: None,
    name: "",
    size: None,
    value: `Null,
  }

  and checkbox = (~value=false, ~formula=?, ~message=?, name) => {
    children: [],
    data: `Null,
    typ: "checkbox",
    formula,
    message: message,
    name: name,
    size: None,
    value: `Bool(value),
  }

  and checkboxes = (~value=0.0, ~formula=?, ~message=?, name, maximum) => {
    children: [number(~value=maximum, name ++ "-max")],
    data: `Null,
    typ: "checkboxes",
    formula,
    message,
    name: name,
    size: None,
    value: `Float(value),
  }

  and heading = (value) => {
    children: [],
    data: `Null,
    typ: "heading",
    formula: None,
    message: None,
    name: "",
    size: None,
    value: `String(value)
  }

  and health = (~formula=?, ~maximum=?, name) => {
    children: [
      number(~value=?maximum, ~formula=?formula, name ++ "-maximum"),
      number(~value=0.0, name ++ "-temporary"),
    ],
    data: `Null,
    typ: "health",
    formula: None,
    message: None,
    name: name,
    size: None,
    value: `Null,
  }

  and horizontalSection = (columns) => {
    let reducer = ((unsizedColumns, specifiedSize: float), section) => {
      switch section.size {
      | None => (unsizedColumns + 1, specifiedSize)
      | Some(size) => (unsizedColumns, specifiedSize +. size)
      }
    }

    let (_unsizedColumns, specifiedSize) = columns |> List.fold_left(reducer, (0, 0.0))
    let defaultSize = (100.0 -. specifiedSize) /. 3.0;
    let children = columns |> List.map(column => {
      ...column,
      size: switch column.size{ | None => Some(defaultSize) | some => some }
    });
    {
      children,
      data: `Null,
      typ: "horizontal-section",
      formula: None,
      message: None,
      name: "",
      size: None,
      value: `Null,
    }
  }


  and message = (name, message) => {
    children: [],
    data: `Null,
    typ: "message",
    formula: None,
    message,
    name: name,
    size: None,
    value: `Null,
  }

  and number = (~formula=?, ~message=?, ~value=?, name) => {
    children: [],
    data: `Null,
    typ: "number",
    formula: formula,
    message: message,
    name: name,
    size: None,
    value: json_float(value),
  }

  and paragraph = (value) => {
    children: [],
    data: `Null,
    typ: "paragraph",
    formula: None,
    message: None,
    name: "",
    size: None,
    value: `String(value)
  }

  and save = (~checked=false, ~message, ~formula, ~value=0.0, name) => {
    children: [
      checkbox(~value=checked, name ++ "-proficiency")
    ],
    data: `Null,
    typ: "save",
    formula,
    message,
    name: name,
    size: None,
    value: `Float(value),
  }

  and section = (~size=?, children) => {
    children: children,
    data: `Null,
    typ: "section",
    formula: None,
    message: None,
    name: "",
    size: size,
    value: `Null,
  }

  and skill = (~checked=0, ~value=0, ~message=?, ~formula=?, ~subtitle="", name) => {

    children: [
      checkbox(~value=(checked >= 1), name ++ "-proficiency"),
      checkbox(~value=(checked >= 2), name ++ "-expertise"),
    ],
    data: `Assoc([("subtitle", `String(subtitle))]),
    typ: "skill",
    formula,
    message,
    name: name,
    size: None,
    value: `Int(value),
  }

  and skill4 = (~checked=0, ~value=0, ~message=?, ~formula=?, ~subtitle="", name) => {
    children: [
      checkbox(~value=(checked >= 1), name ++ "-trained"),
      checkbox(~value=(checked >= 2), name ++ "-expert"),
      checkbox(~value=(checked >= 3), name ++ "-master"),
      checkbox(~value=(checked >= 4), name ++ "-legendary"),
    ],
    data: `Assoc([("subtitle", `String(subtitle))]),
    typ: "skill-4",
    formula,
    message,
    name: name,
    size: None,
    value: `Int(value),
  }

  and tabSection = (value, children) => {
    children,
    data: `Null,
    typ: "tab-section",
    formula: None,
    message: None,
    name: "",
    size: None,
    value: `String(value)
  }

  and table = (headers, children) => {
    children,
    data: `Assoc([("headers", `List(headers |> List.map(str => `String(str))))]),
    typ: "table",
    formula: None,
    message: None,
    name: "",
    size: None,
    value: `Null,
  }

  and text = (~message=?, ~formula=?, name, value) => {
    children: [],
    data: `Null,
    typ: "text",
    formula,
    message,
    name: name,
    size: None,
    value: `String(value)
  }

  and titleSection = (~collapsed=false, value, children) => {
    children,
    data: `Assoc([("collapsed", `Bool(collapsed))]),
    typ: "title-section",
    formula: None,
    message: None,
    name: "",
    size: None,
    value: `String(value)
  }
}

let make_json(~private=true, tabs: list(Property.t)): Yojson.Safe.t = {
  let counter = ref(0)
  let propertiesAsJson: ref(list(Yojson.Safe.t)) = ref([])

  let rec appendList = (properties, parentId) => {
    properties |> List.iteri((idx, property: Property.t) => {
      counter := counter^ + 1;
      propertiesAsJson := [
        `Assoc([
          ("id", `Int(counter^)),
          ("parentId", parentId),
          ("type", `String(property.typ)),

          ("data", property.data),
          ("formula", Util.Json.json_string(property.formula)),
          ("message", Util.Json.json_string(property.message)),
          ("name", `String(property.name)),
          ("size", Util.Json.json_float(property.size)),
          ("value", property.value),

          ("rank", `Int(idx)),
          ("characterId", `Int(1)),
        ]),
        ...propertiesAsJson^
      ]
      appendList(property.children, `Int(counter^))
    })
  }

  appendList(tabs, `Null);

  `Assoc([
    ("properties", `List(propertiesAsJson^)),
    ("private", `Bool(private)),
    ("type", `String("tableplop-character-v2")),
  ])
}
