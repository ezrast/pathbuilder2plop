module Dom = Js_of_ocaml.Dom
module Html = Js_of_ocaml.Dom_html
module Js = Js_of_ocaml.Js
module XmlHttpRequest = Js_of_ocaml_lwt.XmlHttpRequest

let (let.)(fn, lwt) = {
  Lwt.bind(fn, lwt)
}

let fetch(name) = {
  let. resp = XmlHttpRequest.get("data/" ++ name)
  let statusCode = resp.XmlHttpRequest.code
  if (statusCode == 200) {
    let json = Yojson.Safe.from_string(resp.XmlHttpRequest.content);
    Lwt.return(json)
  } else {
    Lwt.fail(Failure("Received status code " ++ string_of_int(statusCode) ++ " fetching " ++ name))
  }
}

let onload(_) = {
  let doc = Html.document
  let appElement = Js.Opt.get(doc##getElementById(Js.string("app")), () => {assert(false)})
  let textArea = Html.createTextarea(doc)
  let button = Html.createButton(doc)
  button##.innerText := Js.string("Convert")
  let textArea2 = Html.createTextarea(doc)
  let errors = Html.createDiv(doc)
  let dlButton = Html.createButton(doc)
  dlButton##.innerText := Js.string("Save as...")

  Dom.appendChild(appElement, textArea)
  Dom.appendChild(appElement, Html.createBr(doc))
  Dom.appendChild(appElement, button)
  Dom.appendChild(appElement, Html.createBr(doc))
  Dom.appendChild(appElement, textArea2);
  Dom.appendChild(appElement, errors);
  Dom.appendChild(appElement, dlButton);

  let store = Nethys.makeStore(fetch)
  button##.onclick := Html.handler(_ => {
    let _ = {
      let result = switch (Util.Json.fromString(textArea##.value |> Js.to_string)) {
      | Error(exn) => Lwt.fail(exn)
      | Ok(json) =>
        try (Lib.Convert.convert(json, store)) {
        | exn => Lwt.fail(exn)
        }
      };

      Lwt.on_failure(result, exn => {
        textArea2##.value := Js.string("")
        errors##.innerText := Js.string(Util.Errors.getExnText(exn))
        errors##setAttribute(Js.string("class"), Js.string("errors"))
      });
      Lwt.bind(result, ((json, warnings)) => {
        textArea2##.value := Js.string(Yojson.Safe.to_string(json));
        errors##.childNodes |> Dom.list_of_nodeList |> List.iter(node => {
          Dom.removeChild(errors, node)
        })
        if (warnings == []) {
          errors##setAttribute(Js.string("class"), Js.string("ok"));
        } else {
          let warningHeader = Html.createP(doc)
          warningHeader##.innerText := Js.string("The following warnings occurred during processing; your sheet may be incomplete:")
          Dom.appendChild(errors, warningHeader)
          warnings |> List.iter(exn => {
            let par = Html.createP(doc)
            par##.innerText := Js.string(Util.Errors.getExnText(exn))
            Dom.appendChild(errors, par)
          });
          errors##setAttribute(Js.string("class"), Js.string("warnings"));
        }
        Lwt.return(())
      })
    };
    Js._false
  })

  dlButton##.onclick := Html.handler(_ => {
    let encodedText = Js.encodeURIComponent(textArea2##.value);
    let anchor = Html.createA(doc)
    anchor##setAttribute(Js.string("href"), Js.string("data:text/plain;charset=utf-8,")##concat(encodedText));
    anchor##setAttribute(Js.string("download"), Js.string("sheet.json"));
    anchor##click;

    Js._false
  })

  Js._false
}

let _ = Html.window##.onload := Html.handler(onload);

