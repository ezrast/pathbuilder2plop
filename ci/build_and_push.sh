#!/bin/bash
set -e

tag=registry.gitlab.com/ezrast/pathbuilder2plop/opam-brotli:debian-ocaml-5.1
docker buildx build -t "$tag" image
# generate a token: https://gitlab.com/-/profile/personal_access_tokens
docker login registry.gitlab.com
docker push "$tag"
