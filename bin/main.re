open Util.Errors

module StringMap = Nethys.StringMap;

let fetchData(name) = {
  let in_channel = open_in("_data/" ++ name)
  let json = Yojson.Safe.from_channel(in_channel);
  Lwt.return(json)
};

let main() = {

  let store = Nethys.makeStore(fetchData);

  let* inChannel = switch Sys.argv {
  | [|_|] => Ok(stdin)
  | [|_, path|] => Ok(open_in(path))
  | _ => Error(Failure("Usage: takes zero or one arguments; reads from the given input file or stdin"))
  }
  let pathbuilderData = Yojson.Safe.from_channel(inChannel);
  let* (sheetJson, warnings) = Util.Errors.exnToResult(() => Lwt_main.run(Lib.Convert.convert(pathbuilderData, store)));
  warnings |> List.iter(exn => {
    prerr_endline(getExnText(exn))
  })
  Ok(Yojson.Safe.pretty_to_channel(stdout, sheetJson))
}

switch (main()) {
| Error(exn) =>
  prerr_endline(getExnText(exn))
  exit(1)
| Ok(()) => exit(0)
}
