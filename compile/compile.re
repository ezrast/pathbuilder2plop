open Util.Json.Infix

module StringMap = Nethys.StringMap

let getArmors(name) = {
  let in_channel = open_in("lfs/" ++ name);
  let json = Yojson.Safe.from_channel(in_channel);

  let hits = json %% "hits" &%% "hits" |> Result.bind(_, Util.Json.requireList) |> Result.get_ok;
  let armorMap = hits |> List.fold_left((acc, hit) => {
    let source = hit %% "_source" |> Result.get_ok;
    let armor = Nethys.armor_of_yojson(source);
    let oldValue = acc |> StringMap.find_opt(armor.name) |> Option.value(~default=[])
    StringMap.add(armor.name, [armor, ...oldValue], acc)
  }, StringMap.empty);
  armorMap
};

let getItems(name) = {
  let in_channel = open_in("lfs/" ++ name);
  let json = Yojson.Safe.from_channel(in_channel);

  let hits = json %% "hits" &%% "hits" |> Result.bind(_, Util.Json.requireList) |> Result.get_ok;
  let itemMap = hits |> List.fold_left((acc, hit) => {
    let source = hit %% "_source" |> Result.get_ok;
    let item = Nethys.item_of_yojson(source);
    let oldValue = acc |> StringMap.find_opt(item.name) |> Option.value(~default=[])
    StringMap.add(item.name, [item, ...oldValue], acc)
  }, StringMap.empty);
  itemMap
};

let getSpells(name) = {
  let in_channel = open_in("lfs/" ++ name);
  let json = Yojson.Safe.from_channel(in_channel);

  let hits = json %% "hits" &%% "hits" |> Result.bind(_, Util.Json.requireList) |> Result.get_ok;
  let spellMap = hits |> List.fold_left((acc, hit) => {
    let source = hit %% "_source" |> Result.get_ok;
    let spell = Nethys.spell_of_yojson(source);
    let oldValue = acc |> StringMap.find_opt(spell.name) |> Option.value(~default=[])
    StringMap.add(spell.name, [spell, ...oldValue], acc)
  }, StringMap.empty);
  spellMap
};

let getWeapons(name) = {
  let in_channel = open_in("lfs/" ++ name);
  let json = Yojson.Safe.from_channel(in_channel);

  let hits = json %% "hits" &%% "hits" |> Result.bind(_, Util.Json.requireList) |> Result.get_ok;
  let itemMap = hits |> List.fold_left((acc, hit) => {
    let source = hit %% "_source" |> Result.get_ok;
    let item = Nethys.weapon_of_yojson(source);
    let oldValue = acc |> StringMap.find_opt(item.name) |> Option.value(~default=[])
    StringMap.add(item.name, [item, ...oldValue], acc)
  }, StringMap.empty);
  itemMap
};

let mergeFn(str, valA, valB) = {
  switch (valA, valB) {
  | (Some(_), Some(_)) => raise(Failure("Duplicate key: " ++ str))
  | (Some(value), None)
  | (None, Some(value)) => Some(value)
  | (None, None) => raise(Failure("Impossible"))
  }
}

let (armors, items, spells, weapons) = try (
  getArmors("armor-2023-07-29.json"),
  getItems("item-2023-07-30.json"),
  getSpells("spell-2023-07-29.json")
  |> StringMap.merge(mergeFn, getSpells("cantrip-2023-07-29.json"))
  |> StringMap.merge(mergeFn, getSpells("focus-2023-07-29.json")),
  getWeapons("weapon-2023-07-28.json")
) {
| exn =>
  prerr_endline(Util.Errors.getExnText(exn));
  exit(1)
}

let ensureDir(path) = try (Unix.mkdir(path, 0o755)) { | Unix.Unix_error(Unix.EEXIST, _, _) => () };
ensureDir("_data");

ensureDir("_data/armor");
armors |> Nethys.StringMap.iter((key, value) => {
  let list = `List(value |> List.map(Nethys.yojson_of_armor))
  Yojson.Safe.to_file("_data/armor/" ++ key ++ ".json", list)
});

ensureDir("_data/item");
items |> Nethys.StringMap.iter((key, value) => {
  let list = `List(value |> List.map(Nethys.yojson_of_item))
  Yojson.Safe.to_file("_data/item/" ++ key ++ ".json", list)
});

ensureDir("_data/spell");
spells |> Nethys.StringMap.iter((key, value) => {
  let list = `List(value |> List.map(Nethys.yojson_of_spell))
  Yojson.Safe.to_file("_data/spell/" ++ key ++ ".json", list)
});

ensureDir("_data/weapon");
weapons |> Nethys.StringMap.iter((key, value) => {
  let list = `List(value |> List.map(Nethys.yojson_of_weapon))
  Yojson.Safe.to_file("_data/weapon/" ++ key ++ ".json", list)
})
